package com.example.eureka.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Value("${eureka.instance.instance-id}")
    private String instanceId;

    @GetMapping("/api/v1/test")
    public ResponseEntity<String> test() {
        return ResponseEntity.ok(String.format("Hello from %s", instanceId));
    }

}
